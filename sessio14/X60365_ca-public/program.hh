
bool buscar(const T &x) const {
    return buscar_aux(primer_node,x);
}

static bool buscar_aux(node_arbreGen* n, const T &x) {
    if (n == NULL) return false;
    else if (n->info == x) return true;
    else {
        int ari = (n->seg).size();
        for (int i = 0; i < ari; ++i) {
            if (buscar_aux(n->seg[i], x))
                return true;
        }
        return false;
    }
}
