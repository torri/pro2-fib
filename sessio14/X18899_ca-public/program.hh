
#include "ArbreN.hh"

static node_arbreNari* sumes_node(node_arbreNari* m ){
        node_arbreNari* n;
	if (m == NULL) n = NULL;
        else {
                n = new node_arbreNari;
                n->info = m ->info;
                int ari = m->seg.size();
                n->seg = vector <node_arbreNari*>(ari);
                for (int i = 0; i < ari; i++){
                        n -> seg[i] = sumes_node(m ->seg[i]);
                        if (n->seg[i]!= NULL){
                                n -> info += (n->seg[i]->info);
                        }

                }


        }
        return n;

}





void arbsuma(ArbreNari& asum) const {
/* Pre: cert */
/* Post: asum és un arbre amb la mateixa estructura que el p.i.
         i cada node és la suma del node corresponent al p.i
         i tots els seus descendents al p.i. */

asum.primer_node = sumes_node(primer_node); 
asum.N = N; 
} 
