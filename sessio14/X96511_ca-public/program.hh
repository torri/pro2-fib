static void freq_aux(node_arbreGen* n, const T &x,int& frequ) {
    
    if (n != NULL) {
        if (n->info == x) frequ++;
    
        int ari = (n->seg).size();
        for (int i = 0; i < ari; ++i) {
            freq_aux(n->seg[i], x,frequ);
        }
       
    }
}
    


int freq (const T &x) const  {
    int frequ = 0; 
    freq_aux(primer_node,x,frequ);
    return frequ;
}
