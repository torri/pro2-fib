
#ifndef _EJERCICIO_LISTAS_
#define _EJERCICIO_LISTAS_

void  reorganitzar_in(const T& x){
/* Pre: p.i. = L */
/* Post: el p.i. conté els mateixos elements que L però
reorganitzats de manera que primer apareixen els més petits
o iguals que x, en el mateix ordre que en L, seguits pels 
més grans que x, en el mateix ordre que en L.
L'element actual del p.i. és el primer del més grans que x, si
existeix, sinó es situa a la dreta de tot */
if (longitud != 0) {

node_llista* a = primer_node;
node_llista* ult_men = NULL;
act = ultim_node = primer_node = NULL;   

while (a != NULL){
    
    if (a->info <= x){
        if(ult_men == NULL){                  //el primero pequeno que se encuentra
            ult_men = a; 
            primer_node = ult_men; 
            primer_node->ant = NULL; 
            
        } else {
            ult_men->seg = a;
            a->ant = ult_men; 
            ult_men = a; 
        }
        
    } else {
        if(ultim_node == NULL){
            ultim_node = a; 
            act = ultim_node; 
            act->ant = NULL;
            
        } else {
            ultim_node->seg = a; 
            a->ant = ultim_node;
            ultim_node = a; 
        }
        
    }
    
    a = a->seg; 
    
}

if (primer_node == NULL){ primer_node = act;
    
} else if (act == NULL){ 
    ultim_node = ult_men;
    
} else {
    ult_men->seg = act; 
    act->ant = ult_men; 
    

}
    ultim_node->seg = NULL; 
}
}

#endif
