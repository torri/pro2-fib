


static node_arbreNari* max_i(node_arbreNari* m) {
/* Pre: el p.i. no és buit */
/* Post: el resultat indica el valor més gran que conté el p.i. */
        node_arbreNari* maximo = m;
        int ari = m->seg.size();
        for (int j = 0; j < ari; ++j) {
            if (m->seg[j] != NULL) {
            node_arbreNari* max_j = max_i(m->seg[j]);
            if (max_j->info > maximo->info)
                maximo = max_j;
            }
        }
    return maximo;

}

T maxim() const {
/* Pre: el p.i. no és buit */
/* Post: el resultat indica el valor més gran que conté el p.i. */
    node_arbreNari* p = max_i(primer_node);
    return p->info;
}

