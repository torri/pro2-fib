
#include <iostream>
#include "ParInt.hh"
#include "BinTree.hh"
#include "BinTreeIOParInt.hh"
using namespace std;

int cerca (const BinTree<ParInt>& a, int x, int& s){
    
    if (!a.empty()){
        if (a.value().primer() == x){
            s = a.value().segon();
            return 0; 
        } 
        int l = cerca (a.left(),x,s);
        if (l != -1){ return 1 + l;}
        int r = cerca(a.right(),x,s);
        if (r != -1){ return 1 + r;}
    }
    return -1;
}

int main (){
    
    BinTree<ParInt> a;
    read_bintree_parint(a);
    
    int x,s;                      //element a cercar 
    while(cin >> x){
        int p = cerca (a,x,s);
        if(p != -1){
           cout << x << ' ' << s << ' '  << p << endl;
        } else {cout << -1 << endl;}
        
    } 
    
}
