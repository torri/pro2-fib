
#include <iostream>
#include "Estudiant.hh"
#include "LlistaIOEstudiant.hh"

int main (){
    
    list <Estudiant> l;
    list <Estudiant>::const_iterator it; 
    LlegirLlistaEstudiant(l);
    
    int num; 
    cin >> num; 
    
    int cont = 0; 
    
    for (it = l.begin(); it != l.end(); ++it){
        if ((*it).consultar_DNI() == num){
        cont++; 
        }
    }
    
    cout << num << " " << cont << endl;
    
}
