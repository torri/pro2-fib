
#include "LlistaIOEstudiant.hh"

void LlegirLlistaEstudiant(list<Estudiant>& l){
    list<Estudiant>::iterator it = l.end();
    Estudiant x; 
  
    x.llegir();
  
    while (x.consultar_DNI()!= 0 && x.consultar_nota() != 0 ) {
    l.insert(it, x);
    x.llegir(); 
    }
}
