
#include <iostream>
#include <list>
#include "ParInt.hh"
#include "LlistaIOParInt.hh"
using namespace std; 



int main(){
     
    list <ParInt> l;
    list <ParInt>::const_iterator it; 
    LlegirLlistaParInt(l);
    
    int num; 
    
    cin >> num; 
    
    int cont = 0;
    int suma = 0; 
    
    for (it = l.begin(); it != l.end(); ++it){
        if ((*it).primer() == num){
        cont++; 
        suma += (*it).segon();
            
        }
    }
    
    cout << num << " " << cont << " " << suma << endl; 
    
}
