
#include "LlistaIOParInt.hh"

void LlegirLlistaParInt(list <ParInt>& l){
    
  list<ParInt>::iterator it = l.end();
  ParInt x; 
  
  x.llegir();
  
  while (x.primer()!= 0 && x.segon() != 0 ) {
    l.insert(it, x);
    x.llegir(); 
  }

}
