
#include <iostream> 
#include <vector> 
using namespace std; 


int solitaris(const vector<int> &v){
     int num = 0; 
     for (int i = 0; i < v.size(); i++){
         if ( (i == 0 && v[i+1] != v[i]) || (((i != (v.size() -1))&&(v[i]!=v[i+1])) && v[i] != v[i-1] ) || (i == (v.size()-1) && v[i] != v[i-1]) ){
             num++; 
        }
    }
    return num; 
}

