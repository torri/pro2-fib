
#include <iostream> 
#include <list>
using namespace std; 


void seleccio(const list<double>& l, list<double>& sel){
    
     list<double>::const_iterator it = l.begin(); 

     double suma = 0; 
     double cont = 1; 
     
     while (it != l.end()){
         
         suma += (*it); 
         double mitjana = suma/cont; 
         
         if((*it) >= mitjana ){
             sel.push_back(*it); 
        }
         ++it; 
         cont++;
    }
}


/*int main (){
    list <double> l; 
    list <double> sel; 
    double a; 
    
    while (cin >> a && a != 0){
        l.push_back(a); 
    }
    
    seleccio(l,sel); 
    
    list<double>::const_iterator it3 = sel.begin(); 
    while (it3 != sel.end()){
        cout << (*it3) << endl; 
        it3++; 
        
    }
}*/
