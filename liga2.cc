#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
 
struct Partit{
    int x;
    int y;
};
struct outputs{
    int name;
    int total;
    int goals;
    int lost;
};
typedef vector <vector <Partit> > scoretable;
 
 
bool comp(const outputs& a, const outputs& b) {
    if (a.total != b.total) return a.total > b.total;
    int ag = a.goals - a.lost;
    int bg = b.goals - b.lost;
    if (ag != bg) return ag > bg;
    return a.name < b.name;
}
 
 
scoretable read_scores() {
    int n;
    cin >> n;
    scoretable score(n,(vector <Partit> (n)));
    for (int i=0; i<n; ++i) for (int j=0; j<n; ++j) cin >> score[i][j].x >> score[i][j].y;
    return score;
}
 
 
vector <outputs> team_scores(const scoretable& score) {
    vector <outputs> out(0);
    int n = score[0].size();
    for (int t=0; t<n; ++t) {
        outputs o;
        o.name = t+1;
        o.total = 0;
        o.goals = 0;
        o.lost = 0;
        for (int i=0; i<n; ++i) {
            if (t!=i) {
                //total
                if          (score[t][i].x > score[t][i].y) o.total += 3;
                else if     (score[t][i].x == score[t][i].y) o.total += 1;
                if          (score[i][t].y > score[i][t].x) o.total += 3;
                else if     (score[i][t].y == score[i][t].x) o.total += 1;
                //goals
                o.goals = o.goals + score[t][i].x + score[i][t].y;
                //lost
                o.lost = o.lost + score[t][i].y + score[i][t].x;
            }
        }
        out.push_back(o);
    }
    sort(out.begin(), out.end(), comp);
    return out;
}
 
 
void print_scores(const vector <outputs>& out) {
    int n = out.size();
    for (int i=0; i<n; ++i) cout << out[i].name << " " << out[i].total << " " << out[i].goals << " " << out[i].lost << endl;
}
 
 
int main() {
    scoretable score = read_scores();
    vector <outputs> out = team_scores(score);
    print_scores(out);
}
