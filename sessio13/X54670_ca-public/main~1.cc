
#include <iostream> 
#include "CuaIOint.hh"
using namespace std; 


int main (){
    Cua <int> c1, c2; 
    llegir_cua_int(c1,0); 
    llegir_cua_int(c2,0);
    
    cout << endl << "Cola c1" << endl; 
    escriure_cua_int(c1); 
    cout << endl << "Cola c2" << endl; 
    escriure_cua_int(c2); 
    cout << endl << "concatenamos c1 y c2" << endl; 
    
    c1.concat(c2); 

    cout << endl << "Cola c1" << endl; 
    escriure_cua_int(c1); 
    cout << endl << "Cola c2" << endl; 
    escriure_cua_int(c2); 
}

