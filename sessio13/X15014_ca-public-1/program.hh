
void arb_sumes(Arbre<int> &asum) const{
/* Pre: cert */
/* Post: l'arbre asum és l'arbre suma del p.i. */

  asum.primer_node = aux(primer_node);
   
}

static node_arbre* aux(node_arbre* a){
    
    node_arbre* n = new node_arbre; 
    
    
    if (a == nullptr) n = nullptr;
    
    else {
        
    n->info = a->info;
    
    
    n->segD = aux(a->segD);
    if (n->segD != nullptr){
        n->info += n->segD->info; 
    }
    
    n->segE = aux(a->segE);
    if (n->segE != nullptr){
        n->info += n->segE->info;
            
        }
    }
    return n; 
    
}
