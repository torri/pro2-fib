
#include <iostream>
using namespace std;

int main (){
    string linea;
    int n, x; 
    cin >> n >> x; 
    for ( int k = 1; k <= n ; k++) {
        
        int suma = 0; 
        int a; 
        
        cin >> a; 
        while ( a != x){
            suma += a; 
            cin >> a; 
        } 
        
        getline(cin, linea);
        
        cout  << "La suma de la secuencia " <<  k << " es " <<  suma << endl;
    }
}
