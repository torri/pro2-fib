
#include "Estudiant.hh"
#include "vectorIOEstudiant.hh"
#include <vector>

/* s'ha d'arrodonir les notes d'un vector d'estudiants*/ 


vector <Estudiant> leer_vector (){
    
    cout << "introduce el num de estudiantes" << endl; 
    int n; 
    cin >> n; 
    vect b (n); 
    for (int i = 0; i < n; i++){
         cout << "Escribe el "<< i+1 << "estudiante <DNI nota>" << endl;
         b[i].llegir(); 
    }
    return b;
}


void redondear_vector(vect& b){
    int n = b.size(); 
    for (int i = 0; i < n; i++){
        b[i].modificar_nota(((int) (10. * (b[i].consultar_nota() + 0.05))) / 10.0);}
    }
    


void escribir_vector(const vect b){
    int n = b.size();
     for (int i = 0; i < n; i++){
        b[i].escriure();}
    }
    
    


int main(){
    
  Estudiant est;
  vect b = leer_vector() ; 
  redondear_vector(b);
  escribir_vector(b);
  
  }
  
