

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


struct resultat {
    int first;
    int second;

};

struct equip{
    int num;
    int punts;
    int marcats;
    int rebuts;
};

typedef vector<vector <resultat>> Matriz;

Matriz llegirmatriu(){
    int n;
    cin >> n;
    Matriz m( n, (vector <resultat> (n)));
    for (int i=0; i < m.size(); ++i) {
      for (int j=0; j < m.size();++j) {
            cin >> m[i][j].first >> m[i][j].second; }
  }
  return m;
}

bool comp (const equip& a, const equip& b){
    if (a.punts != b.punts ){ return a.punts > b.punts;}
    int mra = a.marcats - a.rebuts;
    int mrb = b.marcats - b.rebuts;
    if (mra != mrb) { return mra > mrb;}
    return a.num < b.num;
}


vector <equip> scores (const Matriz& m){
    
    vector <equip> a (0);  
    int k = m[0].size();
                                  

    for (int i = 0;i < k; i++){   
        equip r;                                   /*iniciar equips*/
        r.num = i + 1;
        r.punts = 0;
        r.marcats = 0;
        r.rebuts = 0;
        
        for (int j = 0; j < k; i++){              /*per cada equip*/
            if ( j != i ){
                if (m[i][j].first == m[i][j].second) { r.punts += 1;}
                else if (m[i][j].first > m[i][j].second)  { r.punts += 3;}
                if (m[j][i].first == m[j][i].second) { r.punts += 1;}
                else if (m[j][i].first < m[j][i].second)  { r.punts += 1;}
                r.marcats +=  m[i][j].first + m[j][i].second;
                r.rebuts  +=  m[i][j].second + m[j][i].first;
                
            }
        }
        a.push_back(r);
    }
    sort (a.begin(), a.end(), comp);
    return a;
    
}

void print_scores(const vector <equip>& a) {
    int z = a.size();
    for (int i=0; i<z; ++i) cout << a[i].num << " " << a[i].punts << " " << a[i].marcats << " " << a[i].rebuts << endl;
}


int main (){
    
    Matriz m = llegirmatriu();          //llegim matriu
    vector <equip> a = scores(m);       //es crea vector de resultats fent servir el struct
    print_scores(a);


}
