
#include <iostream>
#include <vector>
#include "Estudiant.hh"


using namespace std;

int main (){
    int m,n,s;
    cin >> m >> n >> s; 
    
    vector <bool> triades (n, false);
    
    for (int i = 0; i < s; i++){
        int triada;
        cin >> triada; 
        triades[triada - 1]= true; 
        }
    
    vector <Estudiant> ma(m); /*vector estudiants*/
    
    for (int i = 0; i < m; i++){
        /*creem estudiant*/
        int dni; 
        cin >> dni;
        ma[i] = Estudiant(dni);
        
//         iniciem mitja
        double mitja = 0; 
       
      
        
        for (int j = 0 ; j < n ; j++){
            
//            anem entrant notes i afegint a la mitja 
            double nota;
            cin >> nota; 
        
            if(triades[j]){
                mitja += nota; 
            } 
            
        
        }
        
        mitja /= s; 
        ma[i].afegir_nota(mitja);
        
        
    }
    
    for (int i = 0; i < m; i++){ ma[i].escriure(); }
    
}
