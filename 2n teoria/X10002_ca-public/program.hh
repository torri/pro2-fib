
void esborrar_tots(const T& x) {
/* Pre: parametre implicit = P */ 
/* Post: s'han eliminat del parametre implicit totes les aparicions d'x (la
    resta d'elements queda en el mateix ordre que a P); si el punt d'interes de P
    referenciava a una aparicio d'x, passa a referenciar al primer element
    diferent d'x posterior a aquesta (si no hi ha cap element diferent d'x, passa
    a la dreta el tot); en cas contrari, el punt d'interes no canvia */

//     cout << a->info << " " << b->info << " " << endl; 
    if (primer_node != NULL){
        node_llista* a = primer_node;           //consulta
        node_llista* b = primer_node;           //últim  consultat que no és x
        while ( a != NULL ){
            if (a->info == x){
//                  cout << " aaa " << endl; 
                
                 if(act == a){
                     act = act->seg; 
                     
                }
                
                node_llista* aux = a; 
                a = a->seg;
                
//                 cout << a->info << " " << aux->info << " " << b->info << endl; 
                
                if (aux == primer_node){
                    
                    b = a;     
                    primer_node = a; 
                    
                } else if (aux == ultim_node){
                
                    ultim_node = b; 
                    b->seg = NULL; 
            
                } else {
                    
                    b->seg = a; 
                    a->ant = b; 
        
                }
                
                longitud--; 
                delete aux; 
//                  cout << "despres " << endl; 
//                  cout << "a " << a->info << " " << a->seg->info << endl; 
//                 cout << "b " << b->info << " " << b->seg->info << endl;
            
            } else {
           
            
            b = a; 
            a = a->seg; 
//             cout << "avansaaaa " << endl; 
//             cout << a->info << " " << b->info << " " << endl; 
            }
        }
        
    }
    
}
