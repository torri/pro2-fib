
#include <iostream>
#include <string> 
#include <map> 
using namespace std; 

int main(){
    char codi; 
    string paraula; 
    map <string, int> partits;
    
     
    while (cin >> codi >> paraula){
        
    if (codi == 'a'){                                //incrementar freqüència de la paraula que ve 
          partits.insert(make_pair(paraula, 0));     //crearlo si no estaba
          map<string, int>::iterator it;     
          it = partits.find(paraula);        
          
          if(it != partits.end()){              
             (*it).second++;
            } 
            
        } else if (codi == 'f'){                      //treure per pantalla la freq de la paraula 
            partits.insert(make_pair(paraula, 0));
            map<string, int>::const_iterator it;       
            it = partits.find(paraula);                
            cout << (*it).second << endl;
        } else if (codi == 'e'){
              map<string, int>::iterator it;     
            it = partits.find(paraula);        
          
            if(it != partits.end()){              
                if ((*it).second != 0){
                    (*it).second--;
                    
                }
            } 
            
            
            
        }
    }
}
