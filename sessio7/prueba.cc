
#include <iostream>
#include <string> 
#include <map>
using namespace std; 

int main(){
    
    map <string, int> nom_2_dni;
    cout << nom_2_dni.size() << endl;   //si se ejecuta te da que es de tamaño vacío porque obvs xD 
    
    //metodología para insertar cosas que no da problemas
    pair <string, int> p1("Joan",1234); //tienes que crear un par con los mismos tipos sino no va lmao
    nom_2_dni.insert(p1);
    cout << nom_2_dni.size() << endl;   //tamaño 1
    //otra cosa que funciona para no ir haciendo variables a lo loco pero es igual de válido
    nom_2_dni.insert(make_pair("Marta",5678));
    nom_2_dni.insert(make_pair("Josep",1111));
    cout << nom_2_dni.size() << endl;   //tamaño 3
    
    //para recorrerlo y eso
    for (map<string, int>::const_iterator it = nom_2_dni.begin() ; it != nom_2_dni.end(); it++){
         /* esto no apunta ni a la clave ni al valor, sinó al par que contiene a los dos */
        cout << (*it).first << ": " << (*it).second << endl;  
    
    }
    //find 
    
    string nombre; 
    cin >> nombre; 
    map<string, int>::const_iterator it;    // se declara el iterador
    it = nom_2_dni.find(nombre);            // haces que apunte al nombre introducido 
                                            // si no existe pues apuntará al final 
    if(it == nom_2_dni.end()){              
            cout << nombre << " no está" << endl; 
    } else {
            cout << nombre << " : " << (*it).second << endl; 
    
    }
    
    //ejemplo de erase: 
    //hay que compilar con la notación de c++11 sinó te da error :( 
    
   
    cin >> nombre;
            /* se podría hacer un find y borrarlo después, pero esta manera también es legal :) */
    int i = nom_2_dni.erase(nombre);        // te interesa saber si se ha borrado bien o no 
    if (i == 0) {                           // si no lo consigue i valdrá 0 
            cout << nombre << " no estaba" << endl; 
        } else {
            cout << nombre << " borrado" << endl; 
        }
    
    //erase 2
    map<string, int>::iterator it2 = nom_2_dni.begin(); 
    while (it2 != nom_2_dni.end()) {
        if(((*it2).second) % 2 == 0){
                it2++;
            } else {
                it2 = nom_2_dni.erase(it2);    
            }
    }
    
    for (map<string, int>::const_iterator it = nom_2_dni.begin() ; it != nom_2_dni.end(); it++){
        cout << (*it).first << ": " << (*it).second << endl; 
    }
    
    nom_2_dni["Pepe"] = 1000;           
    nom_2_dni["Marta"] = 1; 
    
    //el valor anterior (queremos hacer lo mismo pero con cuidao')
    pair<map<string,int>::iterator,bool> r; 

    r = nom_2_dni.insert(make_pair("Marta",1)); // no sobreescribe si ya existía 
    
    (if !r.second){
        cout << "Marta estaba con dni " << (*(r.first)) << endl; 
        } else {
        cout << "insertado" << endl;     
    }
    
}
