
#include "Cjt_estudiants.hh"

void Cjt_estudiants::afegir_estudiant(const Estudiant &est, bool& b){
   // codi de la implementació
  b = false; 
  int i = cerca_dicot(vest,0,nest-1,est.consultar_DNI());
  
  
  //bool = comprovar si ja existeix 
  if (vest[i].consultar_DNI() == est.consultar_DNI()){
      b = true;
  }
      
  //si no existeix, recolocar el vector i insertar nou estudiant     
  if (!b){
      ++nest;
      for (int pos = nest-1; pos > i ; pos--){
          vest[pos] = vest[pos-1];
      }
      vest[i] = est;
      if(est.te_nota()){
          nest_amb_nota++;
          if (nest_amb_nota != 0){
              suma_notes += est.consultar_nota(); 
            } else {
            suma_notes = est.consultar_nota();
        }
      }
      
  } 
}

void Cjt_estudiants::esborrar_estudiant(int dni, bool& b){
    // codi de la implementació
    
    //si no té nota -> no passa res (la mitjana no es modifica)
        // anar copiant i moure-ho tot 
        // reduir nest 
    //si té nota -> recalcular la mitjana 
        //es recoloca 
        // nest-- 
    
    //int i = cerca_dicot(vest,0,nest-1,dni);

    /*if (i < nest && vest[i].consultar_DNI() == dni){
      b = true;
    }*/
    
    b = false; 
    int i = 0; 
    
    while (i < nest && b == false){
        if (vest[i].consultar_DNI() == dni) b = true; 
        ++i; 
    }
    
    if (b){
        
      if(vest[i-1].te_nota()){
          nest_amb_nota--;
          suma_notes -= vest[i-1].consultar_nota();
        }
        
      for (int pos = i; pos < nest ; pos++){
          vest[pos-1] = vest[pos];
      }
      
      nest--;

    } 
}


